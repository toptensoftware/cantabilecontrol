# CantabileControl

## Usage

1. Clone this repository:

        $ git clone https://toptensoftware@bitbucket.org/toptensoftware/cantabilecontrol.git

2. Initialize NPM packages

        $ cd cantabilecontrol
        $ npm install

3. In Cantabile add a MIDI input and a MIDI output ports called "CantabileControl", each mapped to the CantabileControl MIDI device.

4. Run the node script

        $ node index

5. Move UI elements and watch node script output.


## TODO

Lots :)


