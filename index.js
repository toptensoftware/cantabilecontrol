const Cantabile = require('cantabile-js');
const CantabileControl = require('./CantabileControl');

// Main
async function main()
{
    // Connection to Cantabile Network Server
    let C = new Cantabile();
    C.connect();

    // Helper class for communicating with Cantabile Control device
    let CC = new CantabileControl(C);

    // Open required end points
    C.application.open();
    C.bindings.open();

    // Wait till everything connected
    await C.application.untilOpen();
    await C.bindings.untilOpen();

    // Log connected message
    console.log(`Connected to ${C.application.name} ${C.application.edition} build ${C.application.build}`);

    // Reset CantabileControl device state
    CC.reset();
    CC.clearLeds();
    CC.clearOled(true);

    // Hook up event handlers
    CC.on('encoder', (slot, direction) =>  console.log(`Encoder #${slot}: ${direction}`));
    CC.on('encoderButton', (slot) =>  console.log(`Encoder Button #${slot}`));
    CC.on('slider', (slot, value) =>  console.log(`Slider #${slot}: ${value}`));
    CC.on('buttonA', (slot) =>  console.log(`Button A #${slot}`));
    CC.on('buttonB', (slot) =>  console.log(`Button B #${slot}`));
    CC.on('keypad', (button) =>  console.log(`Keypad Button #${button}`));

    // Do some stuff...
    for (let i=1; i<=9; i++)
    {
        CC.setEncoderLed(i, 64);
        CC.setLevelMeterLed(i, 64);
        CC.setOled(i,0,"Hello " + i);
    }
}


main();