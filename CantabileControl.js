const CantabileJS = require('cantabile-js');
const EventEmitter = require('events');

class CantabileControl extends EventEmitter
{
    constructor(CantabileApi, portName)
    {
        super();

        this.CantabileApi = CantabileApi;
        this.portName = portName || "CantabileControl";

        // Open required end points
        this.CantabileApi.application.open();
        this.CantabileApi.bindings.open();

        // Hook up to slot buttons/sliders
        for (let i=1; i<=9; i++)
        {
            // Rotary encoders
            this.watchCC(50+i, value => this.emit('encoder', i, value == 127 ? -1 : 1));

            // Rotary encoder pushed
            this.watchCC(60+i, value => this.emit('encoderButton', i));

            // Sliders 
            this.watchCC(40+i, value => this.emit('slider', i, value));

            // Slot button A
            this.watchCC(0+i, value => this.emit('buttonA', i));

            // Slot button B
            this.watchCC(10+i, value => this.emit('buttonB', i));
        }

        // Hook up to keypad buttons
        for (let i=0; i<12; i++)
        {
            this.watchCC(20+i, value => this.emit('keypad', i));
        }
    }

    // Reset
    reset()
    {
        this.CantabileApi.bindings.invoke(`midiOutputPort.${this.portName}`, [0xB0, 101, 127]);
    }

    // Clear LEDs
    clearLeds()
    {
        this.CantabileApi.bindings.invoke(`midiOutputPort.${this.portName}`, [0xB0, 102, 127]);
    }

    // Clear OLEDs
    clearOled(drawLines)
    {
        this.CantabileApi.bindings.invoke(`midiOutputPort.${this.portName}`, [0xB0, drawLines ? 103 : 104, 127]);
    }

    // Set OLED text
    // Text = Up to 10 ascii characters
    setOled(slot, row, text)
    {
        // Work out CC number
        let cc = 0x30 + slot + (row * 0x10);

        // Sysex header
        let msg = [0xF0, 0x00, cc, 0x00];

        // Ascii data
        let i;
        for (i=0; i<text.length && i<10; i++)
        {
            let ch = text.charCodeAt(i);
            if (ch>0 && ch<=127)
                msg.push(ch);
        }

        // Space pad it
        while (i<10)
        {
            msg.push(0x20);
            i++;
        }

        // Sys-ex terminator
        msg.push(0xF7);

        // Send it
        this.CantabileApi.bindings.invoke(`midiOutputPort.${this.portName}`, msg);
    }

    // "0": Push button
    // "1": Knob
    // "2": Mute Off
    // "3": Mute On
    // "4": Solo Off
    // "5": Solo On
    // "6": Inverted Cross
    // "7": Emdash
    // "8": Blank
    setOledSymbol(slot, char)
    {
        this.CantabileApi.bindings.invoke(`midiOutputPort.${this.portName}`, [0xF0, 0x00, 0x50+slot, 0x00, char.charCodeAt(0), 0xF7]);
    }

    // Set encoder LED
    // Slot = 1..9
    // Value = 0..127
    setEncoderLed(slot, value)
    {
        this.CantabileApi.bindings.invoke(`midiOutputPort.${this.portName}`, [0xB0, 70 + slot, value]);
    }

    // Set level meter LED
    // Slot = 1..9
    // Value = 0..127
    setLevelMeterLed(slot, value)
    {
        this.CantabileApi.bindings.invoke(`midiOutputPort.${this.portName}`, [0xB0 + slot - 1, 81, value])
    }

    // Helper to hook up CC handlers
    watchCC(cc, handler)
    {
        this.CantabileApi.bindings.watch(`midiInputPort.${this.portName}`, null, {
            channel: 0,
            kind: "Controller",
            controller: cc,
        }, handler);
    }
}

module.exports = CantabileControl;