var midi = require('midi');

// Helper to remove the trailing numbers on the port names returned by midi module
function midiCleanName(name)
{
    // Seek backwards past trailing numbers
    let  pos = name.length;
    while (pos-1 > 0 && name[pos-1]>='0' && name[pos-1]<='9')
    {
        pos--;
    }

    // Skip preceding space
    if (pos-1 > 0 && name[pos-1] == ' ')
        pos--;

    return name.substring(0, pos);
}

// Helper to open a MIDI output port with a specific name
function midiOutOpen(name)
{
    // Set up a new output.
    var output = new midi.output();

    for (let i=0; i<output.getPortCount(); i++)
    {
        var portname = output.getPortName(i);
        if (portname == name || midiCleanName(portname) == name)
        {
            output.openPort(i);
            return output;
        }
    }

    return null;
}

// Helper to open a MIDI input port with a specific name
function midiInOpen(name)
{
    // Set up a new output.
    var input = new midi.input();

    for (let i=0; i<input.getPortCount(); i++)
    {
        var portname = input.getPortName(i);
        if (portname == name || midiCleanName(portname) == name)
        {
            input.openPort(i);
            return input;
        }
    }

    return null;
}

class Device
{
    constructor(name)
    {
        if (!name)
            name = "CantabileControl";
     
        this.midiIn = midiInOpen(name);
        if (!this.midiIn)
            throw new Error(`Failed to open MIDI In port ${name}`);

        this.midiOut = midiOutOpen(name);
        if (!this.midiOut)
        {
            this.midiIn.closePort();
            delete this.midiIn;
            throw new Error(`Failed to open MIDI In port ${name}`);
        }

        // Configure a callback for incoming MIDI
        this.midiIn.on('message', function(deltaTime, message) {
            // The message is an array of numbers corresponding to the MIDI bytes:
            //   [status, data1, data2]
            // https://www.cs.cf.ac.uk/Dave/Multimedia/node158.html has some helpful
            // information interpreting the messages.
            console.log('m:' + message);
        });
    }

    // Close device
    close()
    {
        if (this.midiIn)
            this.midiIn.closePort();
        if (this.midiOut)
            this.midiOut.closePort();
        this.midiIn = null;
        this.midiOut = null;
    }

    // Set the OLED display text
    // Slot = 1..9
    // Row = 0..2
    // Text = Up to 10 ascii characters
    setOled(slot, row, text)
    {
        // Work out CC number
        let cc = 0x30 + slot + (row * 0x10);
        if (row > 1)
            cc += 0x10;

        // Sysex header
        let msg = [0xF0, 0x00, cc, 0x00];

        // Ascii data
        let i;
        for (i=0; i<text.length && i<10; i++)
        {
            let ch = text.charCodeAt(i);
            if (ch>0 && ch<=127)
                msg.push(ch);
        }

        // Space pad it
        while (i<10)
        {
            msg.push(0x20);
            i++;
        }

        // Sys-ex terminator
        msg.push(0xF7);

        // Send it
        this.midiOut.sendMessage(msg);
    }

    setOledSymbol(slot, char)
    {
        this.midiOut.sendMessage([0xF0, 0x00, 0x50+slot, 0x00, char.charCodeAt(0), 0xF7]);
    }

    // Set encoder LED
    // Slot = 1..9
    // Value = 0..127
    setEncoderLed(slot, value)
    {
        this.midiOut.sendMessage([0xB0, 70 + slot, value]);
    }

    // Set level meter LED
    // Slot = 1..9
    // Value = 0..127
    setLevelMeterLed(slot, value)
    {
        this.midiOut.sendMessage([0xB0 + slot - 1, 81, value])
    }

    // Reset the device, showing color leds
    reset()
    {
        this.midiOut.sendMessage([0xB0, 101, 127]);
    }

    // Clear LEDs to off
    clearLeds()
    {
        this.midiOut.sendMessage([0xB0, 102, 127]);
    }

    // Clear Displays, optionally drawing horizontal lines
    clearDisplays(drawLines)
    {
        this.midiOut.sendMessage([0xB0, drawLines ? 103 : 104, 127]);
    }

    clearAll()
    {
        this.clearLeds();
        this.clearDisplays(false);
    }
}

module.exports = Device;