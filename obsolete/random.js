var Device = require("./Device");

var d = new Device();

d.reset();

levels = [];
for (let i=0; i<9; i++)
{
    levels[i] = {
        current: 0,
        target: 0,
    };
}

dots = [];
for (let i=0; i<9; i++)
{
    dots[i] = {
        current: 0,
        target: 0,
    };
}

setInterval(function() {

    for (let i=0; i<9; i++)
    {
        if (levels[i].current == levels[i].target)
        {
            levels[i].target = parseInt(Math.random() * 127);
        }
        else
        {
            if (levels[i].current < levels[i].target)
            {
                levels[i].current++;
            }
            else
            {
                levels[i].current--;
            }
            d.setLevelMeterLed(i + 1, levels[i].current);
        }
    }

    for (let i=0; i<9; i++)
    {
        if (dots[i].current == dots[i].target)
        {
            dots[i].target = parseInt(Math.random() * 32);
        }
        else
        {
            if (dots[i].current < dots[i].target)
            {
                dots[i].current++;
            }
            else
            {
                dots[i].current--;
            }
            d.setEncoderLed(i + 1, dots[i].current);
        }
    }

}, 0.05);
